package com.example.p003c2;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modeloo.AlumnosDb;

public class Aplicacion extends Application {
    static ArrayList<Alumno> alumnos;
    private MiAdaptador adaptador;

    private AlumnosDb alumnosDb;

    public ArrayList<Alumno> getAlumnos(){ return alumnos; }

    public MiAdaptador getAdaptador(){ return adaptador; }

    @Override
    public void onCreate(){
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        //alumnos = Alumno.llenarAlumnos();
        alumnos = alumnosDb.allAlumnos();
        alumnosDb.openDataBase();

        adaptador = new MiAdaptador(alumnos, this);
        Log.d("", "onCreate: tamaño array list " + alumnos.size());
    }

}

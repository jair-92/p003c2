package com.example.p003c2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;

import java.io.IOException;
import java.io.InputStream;

import Modeloo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {

    private static final int REQUEST_CODE_PERMISSION = 1;
    private static final int REQUEST_CODE_IMAGE_PICKER = 2;
    private Button btnGuardar, btnRegresar, btnEliminar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String imagePath;
    private int posicion;

    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = findViewById(R.id.btnSalir);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnEliminar = findViewById(R.id.btnEliminar);
        txtMatricula = findViewById(R.id.txtMatricula);
        txtNombre = findViewById(R.id.txtNombre);
        txtGrado = findViewById(R.id.txtGrado);
        imgAlumno = findViewById(R.id.imgAlumno);
        lblImagen = findViewById(R.id.lblFoto);

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if (posicion >= 0 && alumno != null) {
            txtMatricula.setText(alumno.getMatricula());
            txtNombre.setText(alumno.getNombre());
            txtGrado.setText(alumno.getCarrera());

            if (alumno.getImg() != null) {
                imgAlumno.setImageURI(Uri.parse(alumno.getImg()));
                lblImagen.setText(alumno.getImg());
            }
        }

        // Evento clic de la imagen
        imgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posicion >= 0 && alumno != null) {
                    // Modificar Alumno
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setCarrera(txtGrado.getText().toString());

                    // Cargar la imagen si es diferente a nulo
                    if (imagePath != null) {
                        alumno.setImg(imagePath);
                        lblImagen.setText(imagePath);
                    }

                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setMatricula(alumno.getMatricula());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setNombre(alumno.getNombre());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setCarrera(alumno.getCarrera());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setImg(alumno.getImg());

                    alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateAlumno(alumno);
                    Toast.makeText(getApplicationContext(), "Se modificó con éxito ", Toast.LENGTH_SHORT).show();
                } else {
                    // Agregar un nuevo alumno
                    String matricula = txtMatricula.getText().toString();
                    String nombre = txtNombre.getText().toString();
                    String grado = txtGrado.getText().toString();
                    String imagen = imagePath;

                    if (matricula.isEmpty() || nombre.isEmpty() || grado.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    } else {
                        alumno = new Alumno();
                        alumno.setMatricula(matricula);
                        alumno.setNombre(nombre);
                        alumno.setCarrera(grado);
                        alumno.setImg(imagen);

                        alumnosDb = new AlumnosDb(getApplicationContext());

                        // Agregar el alumno nuevo a la bd y obtener el ID que se le asignó
                        long idNuevo = alumnosDb.inserAlumno(alumno);

                        if (idNuevo != -1) {
                            // Asignar el ID al alumno nuevo
                            alumno.setId((int) idNuevo);

                            // Cargar la imagen si es diferente a nulo
                            if (imagePath != null) {
                                alumno.setImg(imagePath);
                                lblImagen.setText(imagePath);
                            }

                            ((Aplicacion) getApplication()).getAlumnos().add(alumno);

                            // Notificar al adaptador del cambio en los datos
                            ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged();

                            setResult(Activity.RESULT_OK);
                            finish();

                            Toast.makeText(getApplicationContext(), "Se guardó con éxito",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Error al guardar alumno", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (posicion >= 0 && alumno != null) {

                    AlertDialog.Builder confirmar = new AlertDialog.Builder(AlumnoAlta.this);
                    confirmar.setTitle("Eliminar Alumnos");
                    confirmar.setMessage("¿Desea eliminar al alumno?");
                    confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());
                            alumnosDb.deleteAlumnos(alumno.getId());

                            ((Aplicacion) getApplication()).getAlumnos().remove(posicion);
                            ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged();

                            setResult(Activity.RESULT_OK);
                            finish();

                            Toast.makeText(getApplicationContext(), "Se eliminó con éxito", Toast.LENGTH_SHORT).show();
                        }
                    });
                    confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //No hace nada
                        }
                    });
                    confirmar.show();

                } else {
                    Toast.makeText(getApplicationContext(), "No se puede eliminar el alumno", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void cargarImagen() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Seleccione la imagen"), REQUEST_CODE_IMAGE_PICKER);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_IMAGE_PICKER) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    try {
                        ContentResolver contentResolver = getContentResolver();
                        imagePath = getPathFromUri(uri);
                        imgAlumno.setImageURI(Uri.parse(imagePath));
                        lblImagen.setText(imagePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private String getPathFromUri(Uri uri) throws IOException {
        if (DocumentsContract.isDocumentUri(this, uri)) {
            String documentId = DocumentsContract.getDocumentId(uri);
            if (isExternalStorageDocument(uri)) {
                String[] split = documentId.split(":");
                if (split.length >= 2) {
                    String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
            } else if (isDownloadsDocument(uri)) {
                Uri contentUri = Uri.parse("content://downloads/public_downloads/" + documentId);
                return getDataColumn(contentUri, null, null);
            } else if (isMediaDocument(uri)) {
                String[] split = documentId.split(":");
                if (split.length >= 2) {
                    String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    String selection = "_id=?";
                    String[] selectionArgs = new String[]{split[1]};
                    return getDataColumn(contentUri, selection, selectionArgs);
                }
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private String getDataColumn(Uri uri, String selection, String[] selectionArgs) {
        ContentResolver contentResolver = getContentResolver();
        String[] projection = {MediaStore.Images.Media.DATA};
        try (Cursor cursor = contentResolver.query(uri, projection, selection, selectionArgs, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                return cursor.getString(columnIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                cargarImagen();
            } else {
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

